package net.cnri.neo4j;

public class RelationshipToId {
    public String name;
    public String id;

    public RelationshipToId(String id, String name) {
        this.name = name;
        this.id = id;
    }
}
