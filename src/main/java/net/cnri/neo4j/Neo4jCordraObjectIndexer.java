package net.cnri.neo4j;

import com.google.gson.*;
import net.cnri.cordra.CordraHooksSupport;
import net.cnri.cordra.CordraHooksSupportProvider;
import net.cnri.cordra.api.*;

import net.cnri.cordra.util.GsonUtility;
import net.cnri.cordra.util.JsonUtil;
import org.neo4j.cypherdsl.core.*;
import org.neo4j.cypherdsl.core.renderer.Configuration;
import org.neo4j.cypherdsl.core.renderer.Renderer;
import org.neo4j.driver.*;
import org.neo4j.driver.Record;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Neo4jCordraObjectIndexer {

    private static CordraHooksSupport hooks = CordraHooksSupportProvider.get();
    private static CordraClient cordra = hooks.getCordraClient();

    private volatile boolean shutdown = false;
    private static Neo4jCordraObjectIndexer instance;
    private volatile Driver driver;

    private volatile Neo4jConfig config;

    private Neo4jCordraObjectIndexer() throws CordraException {
        hooks.addShutdownHook(this::shutdown);
        loadConfigFromDesign();
    }

    private Neo4jCordraObjectIndexer(Neo4jConfig config) {
        loadConfig(config);
    }

    public synchronized static Neo4jCordraObjectIndexer getInstance(Neo4jConfig config) {
        if (instance == null) {
            instance = new Neo4jCordraObjectIndexer(config);
        }
        return instance;
    }
    public synchronized static Neo4jCordraObjectIndexer getInstance() throws CordraException {
        if (instance == null) {
            instance = new Neo4jCordraObjectIndexer();
        }
        return instance;
    }

    private SessionConfig getSessionConfig() {
        if (config.databaseName != null) {
            return SessionConfig.forDatabase(config.databaseName);
        } else {
            return SessionConfig.defaultConfig();
        }
    }

    public synchronized Neo4jConfig loadConfigFromDesign() throws CordraException {
        CordraObject designCo = cordra.get("design");
        Neo4jConfig configToLoad;
        if (designCo.getPayload("neo4jConfig") != null) {
            String configJson = readPayloadToString("design", "neo4jConfig");
            configToLoad = GsonUtility.getGson().fromJson(configJson, Neo4jConfig.class);
        } else {
            configToLoad = new Neo4jConfig();
            configToLoad.user = "neo4j";
            configToLoad.password = "password";
            configToLoad.uri = "bolt://localhost:7687";
        }
        return loadConfig(configToLoad);
    }

    public synchronized Neo4jConfig loadConfig(Neo4jConfig configToLoad) {
        if (driver != null) {
            driver.close();
        }
        AuthToken auth = AuthTokens.basic(configToLoad.user, configToLoad.password);
        this.driver = GraphDatabase.driver(configToLoad.uri, auth);
        this.config = configToLoad;
        return configToLoad;
    }

    private String readPayloadToString(String objectId, String payloadName) throws CordraException {
        try (InputStream in = cordra.getPayload(objectId, payloadName)) {
            String result = new String(in.readAllBytes(), StandardCharsets.UTF_8);
            return result;
        } catch (IOException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    public List<RelationshipToId> getRelationships(CordraObject co, Map<String, JsonElement> pointerToSchemaMap) {
        List<RelationshipToId> result = new ArrayList<>();
        for (Map.Entry<String, JsonElement> entry : pointerToSchemaMap.entrySet()) {
            String pointer = entry.getKey();
            JsonElement subSchemaElement = entry.getValue();
            if (subSchemaElement.isJsonObject()) {
                JsonObject subSchema = subSchemaElement.getAsJsonObject();
                JsonElement handleReferenceElement = JsonUtil.getJsonAtPointer(subSchema, "/cordra/type/handleReference");
                if (handleReferenceElement == null) continue;
                JsonElement referenceInContent = JsonUtil.getJsonAtPointer(co.content, pointer);
                if (referenceInContent.isJsonPrimitive()) {
                    String handle = referenceInContent.getAsString();
                    String relationshipName = relationshipNameFromJsonPointer(pointer);
                    result.add(new RelationshipToId(handle, relationshipName));
                }
            }
        }
        return result;
    }

    public String relationshipNameFromJsonPointer(String jsonPointer) {
        String[] tokens = jsonPointer.split("/");
        for (int i = tokens.length -1; i >= 0; i--) {
            String token = tokens[i];
            if (!isInt(token)) {
                return token;
            }
        }
        return null;
    }

    public boolean isInt(String s) {
        if (s == null) {
            return false;
        }
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private boolean shouldIndexType(String type) {
        if (config.excludeTypes != null) {
            if (config.excludeTypes.contains(type)) {
                return false;
            }
        }
        if (config.includeTypes != null) {
            return config.includeTypes.contains(type);
        } else {
            //index everything
            return true;
        }
    }

    public void deleteAll() {
        deleteAll("CordraObject");
    }

    public void deleteAll(String type) {
        Node match = Cypher.node(type)
                .named("matchNode");
        var statement = Cypher
                .match(match)
                .detachDelete(match)
                .build();

        var renderer = Renderer.getRenderer(Configuration.prettyPrinting());
        String query = renderer.render(statement);
        if (config.verbose) {
            System.out.println();
            System.out.println(query);
        }
        try (Session session = driver.session(getSessionConfig())) {
            session.writeTransaction(tx -> {
                tx.run(query);
                return null;
            });
        }
    }

    public JsonElement search(String cypherQuery) {
        JsonArray jsonRecords = new JsonArray();
        try (Session session = driver.session(getSessionConfig())) {
            Result result = session.run(cypherQuery);
            List<Record> records = result.list();
            Gson gson = GsonUtility.getPrettyGson();
            for (Record r : records) {
                Map<String, Object> map = r.asMap();
                JsonObject jsonRecord = gson.toJsonTree(map).getAsJsonObject();
                jsonRecords.add(jsonRecord);
            }
        }
        return jsonRecords;
    }

    public JsonElement reindexAll(boolean includeRelationships) throws CordraException {
//        long count = 0;
//        try (SearchResults<CordraObject> results = cordra.list()) {
//            count = reindex(results, hooks);
//        }
//        JsonObject json = new JsonObject();
//        json.addProperty("reindexCount", count);
//        return json;
        return reindexQueryResults("*:*", includeRelationships);
    }

    public JsonElement reindexQueryResults(String cordraQuery, boolean includeRelationships) throws CordraException {
        long count = 0;
        try (SearchResults<CordraObject> results = cordra.search(cordraQuery)) {
            count = reindex(results, includeRelationships);
        }
        JsonObject json = new JsonObject();
        json.addProperty("reindexCount", count);
        return json;
    }

    private long reindex(SearchResults<CordraObject> results, boolean includeRelationships) throws CordraException {
        long count = 0;
        for (CordraObject co : results) {
            Map<String, JsonElement> pointerToSchemaMap = hooks.getPointerToSchemaMap(co);
            if (shouldIndexType(co.type)) {
                update(co, pointerToSchemaMap, includeRelationships);
                count++;
            }
        }
        return count;
    }

    public JsonElement reindexId(String id, boolean includeRelationships) throws CordraException {
        CordraObject co = cordra.get(id);
        Map<String, JsonElement> pointerToSchemaMap = hooks.getPointerToSchemaMap(co);
        update(co, pointerToSchemaMap, includeRelationships);
        JsonObject json = new JsonObject();
        json.addProperty("reindexCount", 1);
        return json;
    }

    public void delete(CordraObject co) {
        Node matchNode = Cypher.node("CordraObject")
                .withProperties("_id", Cypher.literalOf(co.id))
                .named("matchNode");
        var statement = Cypher
                .match(matchNode)
                .detachDelete(matchNode)
                .build();

        var renderer = Renderer.getRenderer(Configuration.prettyPrinting());
        String query = renderer.render(statement);
        if (config.verbose) {
            System.out.println();
            System.out.println(query);
        }
        try (Session session = driver.session(getSessionConfig())) {
            session.writeTransaction(tx -> {
                tx.run(query);
                return null;
            });
        }
    }

    public Value update(CordraObject co, Map<String, JsonElement> pointerToSchemaMap, boolean includeRelationships) {
        if (!shouldIndexType(co.type)) return null;
        Object[] keysAndValues = propertiesFor(co, pointerToSchemaMap);
        Node existingNode = Cypher.node(co.type, "CordraObject")
                .withProperties("_id", Cypher.literalOf(co.id))
                .named("existingNode");

        List<RelationshipToId> relationships = Collections.emptyList();
        if (includeRelationships) {
            relationships  = getRelationships(co, pointerToSchemaMap);
        }

        Relationship existingRelationships = existingNode.relationshipTo(Cypher.anyNode()).named("existingRelationships");

        StatementBuilder.OngoingUpdate ongoingUpdate = Cypher
                .merge(existingNode)
                .set(existingNode, Cypher.mapOf(keysAndValues))
                .with(existingNode)
                .optionalMatch(existingRelationships)
                .delete(existingRelationships);

        if (relationships.size() > 0) {
            MatchNodesAndRelationships matchNodesAndRelationships = getMatchNodesAndRelationships(existingNode, relationships);
            for (int i = 0; i < matchNodesAndRelationships.nodeRelationships.size(); i++) {
                PatternElement matchNode = matchNodesAndRelationships.matchNodes.get(i);
                PatternElement nodeRelationship = matchNodesAndRelationships.nodeRelationships.get(i);
                    ongoingUpdate = ongoingUpdate
                            .withDistinct(existingNode)
                            .match(matchNode)
                            .merge(nodeRelationship);
            }
        }
        var statement = ongoingUpdate
                .returning("existingNode")
                .build();

        var renderer = Renderer.getRenderer(Configuration.prettyPrinting());
        String query = renderer.render(statement);
        if (config.verbose) {
            System.out.println();
            System.out.println(query);
        }
        Value resultValue;
        try (Session session = driver.session(getSessionConfig())) {
            resultValue = session.writeTransaction(tx -> {
                @SuppressWarnings("unused") Result result = tx.run(query);
                return null;
                //return result.single().get(0);
            });
        }
        return resultValue;
    }

    public Value create(CordraObject co, Map<String, JsonElement> pointerToSchemaMap) {
        if (!shouldIndexType(co.type)) return null;
        Object[] keysAndValues = propertiesFor(co,pointerToSchemaMap);
        var newNode = Cypher.node(co.type, "CordraObject")
                .withProperties(keysAndValues)
                .named("newNode");

        List<RelationshipToId> relationships = getRelationships(co, pointerToSchemaMap);
        StatementBuilder.OngoingUpdate ongoingUpdate = Cypher
                .create(newNode);

        if (relationships.size() > 0) {
            MatchNodesAndRelationships matchNodesAndRelationships = getMatchNodesAndRelationships(newNode, relationships);
            for (int i = 0; i < matchNodesAndRelationships.nodeRelationships.size(); i++) {
                PatternElement matchNode = matchNodesAndRelationships.matchNodes.get(i);
                PatternElement nodeRelationship = matchNodesAndRelationships.nodeRelationships.get(i);
                ongoingUpdate = ongoingUpdate
                        .withDistinct(newNode)
                        .match(matchNode)
                        .merge(nodeRelationship);
            }
        }

        var statement = ongoingUpdate
                .returning("newNode")
                .build();

        var renderer = Renderer.getRenderer(Configuration.prettyPrinting());
        String query = renderer.render(statement);
        if (config.verbose) {
            System.out.println();
            System.out.println(query);
        }
        Value resultValue;
        try (Session session = driver.session(getSessionConfig())) {
            resultValue = session.writeTransaction(tx -> {
                Result result = tx.run(query);
                return result.single().get(0);
            });
        }
        return resultValue;
    }

    private MatchNodesAndRelationships getMatchNodesAndRelationships(Node newNode, List<RelationshipToId> relationships) {
        MatchNodesAndRelationships matchNodesAndRelationships = new MatchNodesAndRelationships();
        int matchCount = 0;
        for (RelationshipToId relationship : relationships) {
            String id = relationship.id;
            String relationshipName = relationship.name;
            String named = "match" + matchCount;
            matchCount++;
            Node matchNode = Cypher.node("CordraObject")
                    .withProperties("_id", Cypher.literalOf(id))
                    .named(named);
            matchNodesAndRelationships.matchNodes.add(matchNode);
            PatternElement nodeRelationship = newNode.relationshipTo(matchNode, relationshipName);
            matchNodesAndRelationships.nodeRelationships.add(nodeRelationship);
        }
        return matchNodesAndRelationships;
    }

    public synchronized void shutdown() {
        if (shutdown) {
            return;
        }
        shutdown = true;
        if (driver  != null) {
            driver.close();
        }
    }

     public Object[] propertiesFor(CordraObject co, Map<String, JsonElement> pointerToSchemaMap) {
        if ("jsonPointer".equals(config.propertyNameMode)) {
            return jsonPointerPropertiesFor(co, pointerToSchemaMap);
        } else {
            return topLevelPropertiesFor(co);
        }
    }

    public Object[] jsonPointerPropertiesFor(CordraObject co, Map<String, JsonElement> pointerToSchemaMap) {
        List<Object> keysAndValuesList = new ArrayList<>();
        keysAndValuesList.add("_id");
        keysAndValuesList.add(Cypher.literalOf(co.id));
        keysAndValuesList.add("_type");
        keysAndValuesList.add(Cypher.literalOf(co.type));

        JsonElement content = co.content;
        for (Map.Entry<String, JsonElement> entry : pointerToSchemaMap.entrySet()) {
            String pointer = entry.getKey();
            JsonElement value = JsonUtil.getJsonAtPointer(content, pointer);
            if (value.isJsonPrimitive()) {
                Object primitiveValue = getJsonPrimitiveAsPrimitive(value.getAsJsonPrimitive());
                keysAndValuesList.add(pointer);
                keysAndValuesList.add(Cypher.literalOf(primitiveValue));
            }
        }
        Object[] keysAndValues = keysAndValuesList.toArray(new Object[0]);
        return keysAndValues;
    }

    public Object[] topLevelPropertiesFor(CordraObject co) {
        List<Object> keysAndValuesList = new ArrayList<>();
        keysAndValuesList.add("_id");
        keysAndValuesList.add(Cypher.literalOf(co.id));
        keysAndValuesList.add("_type");
        keysAndValuesList.add(Cypher.literalOf(co.type));

        JsonElement content = co.content;
        if (content.isJsonObject()) {
            JsonObject contentObj = content.getAsJsonObject();
            for (Map.Entry<String, JsonElement> entry : contentObj.entrySet()) {
                String key = entry.getKey();
                JsonElement value = entry.getValue();
                if (value.isJsonPrimitive()) {
                    String validName = toValidPropertyName(key);
                    Object primitiveValue = getJsonPrimitiveAsPrimitive(value.getAsJsonPrimitive());
                    keysAndValuesList.add(validName);
                    keysAndValuesList.add(Cypher.literalOf(primitiveValue));
                } else if (value.isJsonArray()) {
                    //TODO do we want to support this?
                    //Neo4j supports the value of a property being an array if the items in the array are all
                    //primitives
                }
            }
        }
        Object[] keysAndValues = keysAndValuesList.toArray(new Object[0]);
        return keysAndValues;
    }

    public Object getJsonPrimitiveAsPrimitive(JsonPrimitive j) {
        Gson gson = new Gson();
        Object result = gson.fromJson(j, Object.class);
        return result;
    }

    public String toValidPropertyName(String name) {
        return name; //TODO do we want to restrict the character set?
    }
}
