package net.cnri.neo4j;

import org.neo4j.cypherdsl.core.PatternElement;

import java.util.ArrayList;
import java.util.List;

public class MatchNodesAndRelationships {

   public List<PatternElement> matchNodes = new ArrayList<>();
   public List<PatternElement> nodeRelationships = new ArrayList<>();
}
